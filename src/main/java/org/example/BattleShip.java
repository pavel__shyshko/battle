package org.example;

import java.util.Arrays;
import java.util.Scanner;

public class BattleShip {
    static String playerName1 = "Player №1";
    static String playerName2 = "Player №2";
    static Scanner scanner = new Scanner(System.in);
    static int[][] battlefield1 = new int[16][16];
    static int[][] battlefield2 = new int[16][16];
    static int[][] monitor1 = new int[16][16];
    static int[][] monitor2 = new int[16][16];

    public static void main(String[] args) {
        System.out.println("Player №1, please enter your name:");
        playerName1 = scanner.nextLine();
        System.out.println("Player №2, please enter your name:");
        playerName2 = scanner.nextLine();
        placeShips(playerName1, battlefield1);
        placeShips(playerName2, battlefield2);
        while (true) {
            makeTurn(playerName1, monitor1, battlefield2);
            if (isWinCondition()) {
                break;
            }
            makeTurn(playerName2, monitor2, battlefield1);
            if (isWinCondition()) {
                break;
            }
        }
    }

    public static void placeShips(String playerName, int[][] battlefield) {
        int deck;
        int time = 0;
        for (deck = 6; deck >= 1; deck--) {
            time++;
            for (int i = 0; i < time; i++) {
                System.out.println(playerName + ", please place your " + deck + "-deck ship on the battlefield:");
                System.out.println();
                drawField(battlefield);
                String[] abc = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P"};
                System.out.println("Please enter X coordinate:");
                int x = Arrays.asList(abc).indexOf(scanner.next().toUpperCase());
                System.out.println("Please enter Y coordinate:");
                int y = scanner.nextInt();
                System.out.println("Choose direction:");
                System.out.println("1. Vertical.");
                System.out.println("2. Horizontal.");
                int direction = scanner.nextInt();
                if (!isAvailable(x, y, deck, direction, battlefield)) {
                    System.out.println("Wrong coordinates!");
                    continue;
                }
                for (int k = 0; k < deck; k++) {
                    if (direction == 1) {
                        battlefield[x][y + k] = 1;
                    } else {
                        battlefield[x + k][y] = 1;
                    }
                }
            }
        }
    }

    public static void drawField(int[][] battlefield) {
        System.out.println("   A B C D E F G H I J K L M N O P");
        for (int i = 0; i < battlefield.length; i++) {
            System.out.print((i < 10) ? " " + i + " " : i + " ");
            for (int j = 0; j < battlefield[1].length; j++) {
                if (battlefield[j][i] == 0) {
                    System.out.print("- ");
                } else {
                    System.out.print("X ");
                }
            }
            System.out.println();
        }
    }

    public static void makeTurn(String playerName, int[][] monitor, int[][] battlefield) {
        while (true) {
            System.out.println(playerName + ", please, make your turn.");
            System.out.println("   A B C D E F G H I J K L M N O P");
            for (int i = 0; i < monitor.length; i++) {
                System.out.print((i < 10) ? " " + i + " " : i + " ");
                for (int j = 0; j < monitor[1].length; j++) {
                    if (monitor[j][i] == 0) {
                        System.out.print("- ");
                    } else if (monitor[j][i] == 1) {
                        System.out.print(". ");
                    } else {
                        System.out.print("X ");
                    }
                }
                System.out.println();
            }
            String[] abc = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P"};
            System.out.println("Please enter X coordinate:");
            int x = Arrays.asList(abc).indexOf(scanner.next().toUpperCase());
            System.out.println("Please enter Y coordinate:");
            int y = scanner.nextInt();
            if (battlefield[x][y] == 1) {
                System.out.println("Hit! Make your turn again!");
                monitor[x][y] = 2;
            } else {
                System.out.println("Miss! Your opponents turn!");
                monitor[x][y] = 1;
                break;
            }
        }
    }

    public static boolean isWinCondition() {
        int counter1 = 0;
        for (int i = 0; i < monitor1.length; i++) {
            for (int j = 0; j < monitor1[i].length; j++) {
                if (monitor1[i][j] == 2) {
                    counter1++;
                }
            }
        }
        int counter2 = 0;
        for (int i = 0; i < monitor2.length; i++) {
            for (int j = 0; j < monitor2[i].length; j++) {
                if (monitor2[i][j] == 2) {
                    counter2++;
                }
            }
        }

        if (counter1 >= 56) {
            System.out.println(playerName1 + " WIN!!!");
            return true;
        }
        if (counter2 >= 56) {
            System.out.println(playerName2 + " WIN!!!");
            return true;
        }
        return false;
    }

    public static boolean isAvailable(int x, int y, int deck, int rotation, int[][] battlefield) {
        if (isInBound(x, y, deck, rotation, battlefield) && isNoNeighbourShips(x, y, deck, rotation, battlefield)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isInBound(int x, int y, int deck, int rotation, int[][] battlefield) {
        if (rotation == 1) {
            if (y + deck > battlefield.length) {
                return false;
            }
        }
        if (rotation == 2) {
            if (x + deck > battlefield[0].length) {
                return false;
            }
        }
        return true;
    }

    public static boolean isNoNeighbourShips(int x, int y, int deck, int rotation, int[][] battlefield) {
        while (deck != 0) {
            for (int i = 0; i < deck; i++) {
                int xi = 0;
                int yi = 0;
                if (rotation == 1) {
                    yi = i;
                } else {
                    xi = i;
                }
                if (x + 1 + xi < battlefield.length && x + 1 + xi >= 0) {
                    if (battlefield[x + 1 + xi][y + yi] != 0) {
                        return false;
                    }
                }
                if (x - 1 + xi < battlefield.length && x - 1 + xi >= 0) {
                    if (battlefield[x - 1 + xi][y + yi] != 0) {
                        return false;
                    }
                }
                if (y + 1 + yi < battlefield.length && y + 1 + yi >= 0) {
                    if (battlefield[x + xi][y + 1 + yi] != 0) {
                        return false;
                    }
                }
                if (y - 1 + yi < battlefield.length && y - 1 + yi >= 0) {
                    if (battlefield[x + xi][y - 1 + yi] != 0) {
                        return false;
                    }
                }
            }
            deck--;
        }
        return true;
    }
}

